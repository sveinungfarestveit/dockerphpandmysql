<!doctype html>
<html lang="en">
    <head>
        <title>Worker</title>
        <link rel="stylesheet" href="/styles.css">
        <?php require_once __DIR__ . "/../main.php"; ?>
    </head>
    <body>

        <?php
            echo createToolbar(App::$PAGES, "worker");
        ?>
        <h1>Worker</h1>

        <button onclick="showHide('worker-form')">New worker</button>

        <?php 
            echo createHiddenForm(App::class, "worker-form", "Worker", "worker", [ "Name", "Email:email" ]);
            
            echo createHtmlTable(App::class, "select * from Worker ORDER BY Name", "liste", "worker-form", "Name", "Email");
        ?>

    </body>

</html>