<!doctype html>
<html lang="en">
    <head>
        <title>Project</title>
        <link rel="stylesheet" href="/styles.css">
        <?php require_once __DIR__ . "/../main.php"; ?>
    </head>
    <body>

        <?php
            echo createToolbar(App::$PAGES, "project");
        ?>
        <h1>Projects</h1>

        <button onclick="showHide('project-form')">New project</button>

        <?php 
            echo createHiddenForm(App::class, "project-form", "Project", "project", [ "Name", "Description", "WorkflowID(Workflow.ID, Workflow.Name) => Workflow" ]);
            
            echo createHtmlTable(App::class, "select P.*, W.Name as Workflow"
                . ", (SELECT Count(*) FROM Task WHERE ProjectID = P.ID) as TaskCount"
                . " FROM Project P"
                . " LEFT JOIN Workflow W ON W.ID = P.WorkflowID"
                . " ORDER BY P.Name", "liste", "project-form", 
                "Name", "Description", "Workflow(WorkflowID) => Status", "TaskCount => Tasks");
        ?>

    </body>

</html>