# PHP and MySql Example #

Php with Mysql running in Docker

### What is this repository for? ###

Example of project combining PHP and Mysql in a docker container

### How do I get set up? ###

#### Prepare:
* Install Docker Desktop
* Create new passwords in: ```docker-compose.yml```
* Update the password in: ```www/appsettings.php```

#### Run:
* open command-line and type: ```docker-compose build```
* after it finishes you type: ```docker-compose up -d```
* navigate your browser to: http://localhost:8080
