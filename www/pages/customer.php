<!doctype html>
<html lang="en">
    <head>
        <title>Customers</title>
        <link rel="stylesheet" href="/styles.css">
        <?php require_once __DIR__ . "/../main.php"; ?>
    </head>
    <body>

        <?php
            echo createToolbar(App::$PAGES, "customer");
        ?>
        <h1>Customers</h1>

        <button onclick="showHide('customer')">New customer</button>

        <?php 
            echo createHiddenForm(App::class, "customer", "Customer", "customer", 
                [ "Name", "Address", "PostalCode", "City", "Email", "Phone" ]);

            echo createHtmlTable(App::class, "select C.ID, C.Name, C.Address, C.PostalCode, C.City, C.Email, C.Phone"
                . ", Sum(P.Price * O.Quantity) as OrderSum"
                . " FROM `Customer` C"
                . " LEFT JOIN `Order` O ON O.CustomerID = C.ID"
                . " LEFT JOIN `Product` P ON P.ID = O.ProductID"
                . " GROUP BY C.ID, C.Name, C.Address, C.PostalCode, C.City, C.Email, C.Phone"
                . " ORDER BY Name", 
                "liste", "customer" , 
                "Name", "Address", "PostalCode", "City", "Email", "Phone", "OrderSum:money => Sum orders");
        ?>

    </body>

</html>