<!doctype html>
<html lang="en">
    <head>
        <title>Task</title>
        <link rel="stylesheet" href="/styles.css">
        <?php require_once __DIR__ . "/../main.php"; ?>
    </head>
    <body>

        <?php
            echo createToolbar(App::$PAGES, "task");
        ?>
        <h1>Tasks</h1>

        <section class="action-bar">
            <button onclick="showHide('task-form')">New task</button>
            <?php 
                $cb = createFilterDropdown(App::GetDB(), "SELECT ID, Name FROM Project ORDER BY Name", 
                    "projectid", "location = '?projectid=' + this.value;");
                $filter = $cb["selected"] !== "" ? "T.ProjectID = " . $cb["selected"] : "T.ID > 0";
                echo $cb["html"];
            ?>
        </section>

        <?php 
            echo createHiddenForm(App::class, "task-form", "Task", "task", [ "Title", "Description", "WorkflowID(Workflow.ID, Workflow.Name) => Workflow", "ProjectID(Project.ID, Project.Name) => Project" ]);
            
            echo createHtmlTable(App::class, "select P.Name as ProjectName, W.Name as Workflow, T.*"
                . " FROM Task T"
                . " LEFT JOIN Project P ON P.ID = T.ProjectID"
                . " LEFT JOIN Workflow W ON W.ID = T.WorkflowID"
                . " WHERE " . $filter
                . " ORDER BY ProjectID DESC, T.ID DESC", "liste", "task-form", 
                "Title", "Description", "ProjectName(ProjectID) => Project", "Workflow(WorkflowID)");
        ?>

    </body>

</html>