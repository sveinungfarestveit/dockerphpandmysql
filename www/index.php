<!doctype html>
<html lang="en" >
	<head>
		<title>EasyCrm</title>
		<link rel="stylesheet" href="styles.css">
	</head>
	<body>
		<?php 
			require_once "main.php"; 		
			echo createToolbar(App::$PAGES, "index");
		?>

		<h1>
			<?= App::$LANG["app-title"]?>
		</h1>

		<p>
			<?= App::$LANG["app-slogan"]?>
		</p>
		
	</body>
</html>