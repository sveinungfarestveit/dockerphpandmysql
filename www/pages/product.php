<!doctype html>
<html lang="en">
    <head>
        <title>Products</title>
        <link rel="stylesheet" href="/styles.css">
        <?php require_once __DIR__ . "/../main.php"; ?>
    </head>
    <body>

        <?php
            echo createToolbar(App::$PAGES, "product");
        ?>
        <h1>Products</h1>

        <button onclick="showHide('product')">New product</button>

        <?php 
            echo createHiddenForm(App::class, "product", "Product", "product", [ "Code => Product code", "Name", "Description", "Price" ]);
            
            echo createHtmlTable(App::class, "select * from Product ORDER BY Name", "liste", "product",
                "Code => Product code", "Name", "Description", "Price:money");
        ?>

    </body>

</html>