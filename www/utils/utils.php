<?php

    function getItem($value, $getFirst = true, $default = "", $divider = "=>") {
        $pos = stripos($value, $divider);
        if ($pos > 0 ) {
           if ($getFirst) return trim(substr($value, 0, $pos));
           return trim(substr($value, $pos + strlen($divider)));
        }
        return $getFirst ? $value : $default;
    }

    /// ======================================================
    /// createHiddenForm(id, name, [array of inputfields] )
    ///  helper function that creates a hidden form
    /// ======================================================    
    function createHiddenForm($app, $id, $name, $title, $fields) {

        $action = htmlspecialchars($_SERVER["PHP_SELF"]);
        $lang = $app::$LANG;
        $cn = $app::GetDB();

        $ht = "<section class='dialog' id='$id' style='display: none'>"
            . "<header data-id='title' data-title='$title'>$title</header>"
            . "<form autocomplete='off' name='$name' method='POST' action='$action'>"
            . "<input type='hidden' name='FormName' value='$name'>"
            . "<input type='hidden' name='ID' value='0'>"
            . "<input type='hidden' name='action' value='save'>";

        foreach ($fields as $fld) {
            $lookup = createLookup($cn, $fld);
            if ($lookup !== "") {
                $ht .= $lookup;
            } else {
                $fname = getItem($fld, true);
                $type = getItem($fname, false, "text", ":");
                $fname = $type === "" ? $fname : getItem($fname, true, "", ":");
                $label = getItem($fld, false, $fname, "=>");
                $ht .= "<label for='$fname'>$label</label>";
                $evt = "";
                switch ($type) {
                    case "money":
                        $type = "number";
                        $evt = " onchange='setTwoNumberDecimal' step='0.01'";
                        break;
                }
                $ht .= "<input type='$type' $evt name='$fname'>";
            }
        }            
        
        $ht .= "<footer class='buttons'>" 
            . "<input style='grid-area:b' type='submit' name='$name' value='" . $lang["button-ok"] . "'>"
            . "<button style='grid-area:a; display: none' class='bad delete'"
            . "  onclick=\"return setFormAction('$id','delete');\">" . $lang["button-delete"] . "</button>"
            . "<button style='grid-area:c' class='warn'"
            . "  onclick=\"return showHide('$id');\">" . $lang["button-cancel"] . "</button>"
            . "</footer>"
            . "</form>"
            . "</section>";
        return $ht;
    }
    
    /// =========================================================
    /// createLookup( cn, fld )
    ///  helper function that creates html for a dropdown (combo)
    ///  returns empty if there is no parenthesis in the fld
    /// =========================================================     
    function createLookup($cn, $fld, $script = "") {
        // eks: LagID(Lag.ID,Lag.Navn) => Lag
        $ht = "";
        $label = getItem($fld, false, "");
        $setup = getItem(getItem($fld, true), false, "", "(");
        if ($setup !== "") {            
            $name = getItem($fld, true, "", "(");
            $setup = substr($setup, 0, strlen($setup) - 1 );
            $fields = explode(",", $setup);
            $table = "";
            $select = [];
            foreach ($fields as $field) {
                $parts = explode(".", $field);
                if ($table === "") { $table = $parts[0]; }
                array_push($select, $parts[1]);
            }
            $sql = "SELECT " . implode(", ", $select) . " FROM `$table`";
            $rows = $cn->query($sql);
            $ht = "<label for='$name'>$label</label>";
            $evt = $script !== "" ? (" onChange=\"" . $script . "\" ") : "";
            $ht .= "<select name='$name' id='$name'$evt>";
            while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
                $ht .= "<option value='" . $row[$select[0]] . "'>" . $row[$select[0]] . " - " . $row[$select[1]] . "</option>";
            }
            $ht .= "</select>";
        }
        return $ht;
    }

    function createFilterDropdown($cn, $sql, $urlparam = "", $script = "") {
        $keyFound = false;
        $firstKey = "";
        $key = array_key_exists($urlparam, $_GET) ? $_GET[$urlparam] : "";
        $pkey = get_array_ikey($urlparam, $_POST);
        if ($pkey !== false) { $key = $_POST[$pkey]; }
        $rows = $cn->query($sql);
        $evt = $script !== "" ? (" onChange=\"" . $script . "\" ") : "";
        $ht = "<select $evt>";
        while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
            $htSelected = "";
            $rowID = $row["ID"];
            if ($rowID == $key) {
                $htSelected = "selected";
                $keyFound = true;
            }
            if ($firstKey === "") {
                $firstKey = $rowID;
            }
            $ht .= "<option value='" . $rowID . "'$htSelected>" . $row["Name"] . "</option>";
        }
        $ht .= "</select>";
        return [ "html" => $ht, "selected" => $keyFound ? $key : $firstKey ] ;
    }

    /// ======================================================
    /// createToolbar([pages-array], name-of-current-page )
    ///  helper function that creates a html-toolbar with links to pages
    /// ======================================================    
	function createToolbar($pages, $selected) {
		$ht = "<table class='toolbar'><tr>";
		foreach ($pages as $key => $page) {
			$url = $key;
			$label = $page;
			$class = (stripos($url, $selected . ".php") !== False) ? "selected" : "";
			$ht .= "<td>";
			$ht .= "<a class='$class' href='/$url'>$label</a>";
			$ht .= "</td>";				
		}
		$ht .= "</tr></table>";
		return $ht;
	}    

    /// ======================================================
    /// createHtmlTable(cn, sql, className, colName1, colName2 ... )
    ///  helper function that runs a query and returns a html table
    /// ======================================================
    function createHtmlTable($app, $sql, $className, $formName, string ...$columns) {
        $lang = $app::$LANG;
        $conn = $app::GetDB();        
        $html = "<table class='$className'>";
        $html .= "<tr>";
        $layout = parseTableLayout($columns);
        foreach ($layout as $key => $col) {
            $label = $col["label"];
            $html .= "<th class='$key left'>$label</th>";
        }
        $html .= "</tr>";
        $rowCount = 0;
        $rows = $conn->query($sql);
        while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
            $id = $row["ID"];
            $html .= "<tr class=\"edit-link\" onClick=\"showEdit('$formName', $id, event)\">";
            foreach ($layout as $fieldName => $col) {
                // Add foreign-key into data-id attribute ?
                $foreignKey =  $col["foreignKey"];
                if ($foreignKey !== "") {
                    $foreignKey = "data-id='" . $row[$foreignKey] . "' data-name='" . $foreignKey . "'";
                }
                $value = $row[$fieldName];
                $format = $col["format"];
                $classList = [$fieldName];
                switch ($format) {
                    case "money":
                        $value = number_format($value, 2, '.', ' ');
                        array_push($classList, "right");
                        array_push($classList, $value >= 0 ? "plus" : "minus");
                        break;
                }
                // Cell html:
                $classes = count($classList) > 0 ? "class='" . join(" ", $classList) . "'" : "";
                $html .= "<td $foreignKey $classes>$value</td>";
                $rowCount++;
            }
            $html .= "</tr>";
        }
        if ($rowCount == 0) {
            $infoMsg = $lang["no-entries"];
            $html .= "<tr><td colspan='" . count($columns) . "' style='color: #aaa'>$infoMsg</td></tr>";
        }
        $html .= "</table>";
        return $html;
    }

    function parseTableLayout($columns) {
        // example [ "OrderDate:date", "CustomerName(CustomerID) => Customer", "ProductCode(ProductID) => Product", "ProductName", "Quantity:money", "CreatedAt", "UpdatedAt" ]
        $layout = [];
        foreach ($columns as $column) {
             $fieldName = getItem($column, true, "", "=>");
             $label = getItem($column, false, "***", "=>");
             $foreignKey = getItem($fieldName, false, "", "(");
             if ($foreignKey !== "") {
                 $fieldName = getItem($fieldName, true, "", "(");
                 $foreignKey = substr($foreignKey, 0, strlen($foreignKey) - 1 );
             }
             $format = getItem($fieldName, false, "", ":");
             if ($format !== "") {
                 $fieldName = getItem($fieldName, true, "", ":");
             }
             if ($label === "***") {
                $label = getItem($column, false, $fieldName, "=>");
             }                     
             $layout[$fieldName] = [ "label" => $label, "format" => $format, "foreignKey" => $foreignKey ];
        }
        return $layout;
    }

    function get_array_ikey($needle, $haystack) {
        foreach ($haystack as $key => $meh) {
            if (strtolower($needle) == strtolower($key)) {
                return (string) $key;
            }
        }
        return false;
    }

    function normalizeDecimal($val): string
    {
        $val = str_replace(" ","", $val);
        $val = str_replace(",",".", $val);
        return floatval($val);
    }    


?>

