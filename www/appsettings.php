<?php

    class App {

        public static $DB_Server = "mysql";
        public static $DB_UserName = "app";
        public static $DB_DataBase = "app";
        public static $DB_Password = "****";

        private static $db_connection = null;

        public static $TABLES = [

            "Worker" => [
                "Name" => "varchar(100)",
                "Email" => "varchar(100)",
                "IsActive" => "TINYINT(1) default 1"
            ],

            "Workflow" => [
                "Step" => "int default 1",
                "Name" => "varchar(50)"
            ],

            "Project" => [
                "Name" => "varchar(100)",
                "Description" => "varchar(500)",
                "WorkflowID" => "int NOT NULL => Workflow(ID)",
                "ParentProjectID" => "int => Project(ID)"
            ],            

            "Task" => [
                "Title" => "varchar(200)",
                "Description" => "text",
                "WorkflowID" => "int NOT NULL => Workflow(ID)",
                "StoryPoints" => "int default 1",
                "ProjectID" => "int NOT NULL => Project(ID)",
                "ParentTaskID" => "int => Task(ID)",
                "AssignedTo" => "int => Worker(ID)"
            ],

            "Customer" => [ 
                "Name" => "varchar(100)", 
                "Address" => "varchar(300)", 
                "City" => "varchar(100)", 
                "PostalCode" => "varchar(20)", 
                "Email" => "varchar(100)",
                "Phone" => "varchar(20)"
            ],
            "Product" => [ 
                "Code" => "varchar(20)", 
                "Name" => "varchar(100)", 
                "Description" => "varchar(500)", 
                "Price" => "DECIMAL(19 , 4 )"
            ],
            "Order" => [ 
                "OrderDate" => "DATE", 
                "CustomerID" => "int NOT NULL => Customer(ID)", 
                "ProductID" => "int NOT NULL => Product(ID)", 
                "Quantity" => "INT"
            ]
        ];

        public static $PAGES = [
            "index.php" => "Home",
            "pages/worker.php" => "Workers",
            "pages/workflow.php" => "Workflow",
            "pages/project.php" => "Projects",
            "pages/task.php" => "Tasks",
            "pages/customer.php" => "Customers",
            "pages/product.php" => "Products",
            "pages/order.php" => "Orders"
        ];

        public static $LANG = [
            "app-title" => "Super Easy CRM!",
            "app-slogan" => "Simple CRM solution for your intranet!",

            "button-ok" => "OK",
            "button-cancel" => "Cancel",
            "button-delete" => "Delete",
            "no-entries" => "Found no elements to show. Maybe you could create one ?"
        ];

        public static function GetDB() {
            if (self::$db_connection == null) {
                $conn = new mysqli(self::$DB_Server, self::$DB_UserName, self::$DB_Password, self::$DB_DataBase);
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }    
                self::$db_connection = $conn;            
            }
            return self::$db_connection;
        }

    }

?>

    <script>

        var app = {
            lang: {
                edit: "Edit",
                create: "Create"                
            }
        }

    </script>