<?php

    require_once "utils.php";
    require_once __DIR__ . "/../appsettings.php";

    // Autocreate missing tables
    foreach (App::$TABLES as $name => $columns) {       
        checkDatabaseTable(App::GetDB(), $name, $columns);
    }

    // POST (save data?)
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (array_key_exists("FormName", $_POST)) {
            $tableName = $_POST["FormName"];            
            if (array_key_exists($tableName, App::$TABLES)) {
                $columns = App::$TABLES[$tableName];
                saveToDatabaseTable(App::GetDB(), $tableName, $columns, $_POST);
            }
        }
    }

    /// ======================================================
    /// saveToDatabaseTable(cn, tablename, tableInfo, formdata )
    ///  helper function that saves formvalues to the database
    ///  (table and fields must be predefined in the appsettings.php)
    /// ======================================================    
    function saveToDatabaseTable($cn, $name, $columns, $form) {
        $colNames = "";
        $values = "";
        $params = [];
        $colnameArray = [];
        $pos = 0;
        $paramTypes = "";
        $id = 0;
        $action = "save";

        if (array_key_exists("ID", $form)) {
            $id = intval($form["ID"]);
        }

        if (array_key_exists("action", $form)) {
            $action = $form["action"];
        }        
        
        foreach ($columns as $colName => $typeInfo) {
            if (array_key_exists($colName, $form)) {
                $pos++;
                if ($pos > 1) { $colNames .= ", "; $values .= ", "; }
                $colNames .= $colName;
                array_push($colnameArray, $colName);
                $value = strip_tags($form[$colName]);
                switch (getItem($typeInfo, true, "", "("))
                {
                    case "INT":
                        $values .= "?";
                        array_push($params, intval($value));
                        $paramTypes .= "i";
                        break;

                    case "DATE":
                        $values .= "?";
                        array_push($params, date('Y/m/d', strtotime($value)));
                        $paramTypes .= "s";                        
                        break;

                    default:
                        $values .= "?";
                        array_push($params, $value);
                        $paramTypes .= "s";
                        break;
                }                
            }
        }

        // Insert (default):
        $sql = "INSERT INTO `$name` ($colNames) values ($values)";

        // Update ?
        if ($id > 0 ) {
            $sql = "UPDATE `$name` SET ";
            $colIndex = 0;
            foreach ($colnameArray as $col) {
                if ($colIndex > 0) $sql .= ", ";
                $sql .= "$col = ?";
                $colIndex++;
            }

            $sql .= " WHERE ID = $id";
        }

        // Delete ?
        if ($action === "delete") {
            $sql = "DELETE FROM `$name` WHERE ID = $id";
            $params = [];
        }

        $stmt = $cn->prepare($sql);
        if ($stmt === FALSE) {
            echo "<p><code>$sql</code></p>";
            die('prepare() failed: ' . htmlspecialchars($cn->error));
        }
        
        if (count($params) > 0) {
            $stmt->bind_param($paramTypes, ...$params);
        }
        $stmt->execute();
        $stmt->close();
        

    }

    /// ======================================================
    /// checkTable(cn, tablename, [colName => type] ... )
    ///  helper function that autocreate missing tables
    /// ======================================================
    function checkDatabaseTable($conn, $name, $columns) {
        $sqlColumns = "";
        $foreignKeys = [];
        $foreignKeyColumns = [];
        $index = 0;

        foreach ($columns as $colName => $typeInfo) {
            if ($index++ > 0) { $sqlColumns .= ","; }
            // Foreign key?
            $fkPos = stripos($typeInfo, "=>");
            if ($fkPos > 0 ) {
                $fkInfo = trim(substr($typeInfo, $fkPos + 2));
                $typeInfo = trim(substr($typeInfo, 0, $fkPos));
                array_push($foreignKeys, $fkInfo);
                array_push($foreignKeyColumns, $colName);
            }
            $sqlColumns .= "`" . $colName . "` " . $typeInfo;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `$name` ("
            . "`ID` int NOT NULL auto_increment,"
            . "$sqlColumns,"
            . "CreatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,"
            . "UpdatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,"
            . "PRIMARY KEY (`ID`)";

        for ($fkix = 0; $fkix < count($foreignKeys); $fkix++) {
            $sql .= ", FOREIGN KEY (" . $foreignKeyColumns[$fkix] . ") REFERENCES " . $foreignKeys[$fkix];
        }

        $sql .= " );";

        if(!$conn->query($sql)){
            echo "Table ($name) creation failed: (" . $conn->errno . ") " . $conn->error;
            echo "<div>" . $sql . "</div>";
            return false;
        } else {
            return true;
        }
    }

?>