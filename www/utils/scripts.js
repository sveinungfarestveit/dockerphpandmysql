
    // Re-post blocker:
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }


    function showHide(skjemaId, isEdit) {
        var el = document.getElementById(skjemaId);
        if (el) {
            if (el.style.display === "none") {
                if (!isEdit) {
                    setFormRecordID(skjemaId, 0);
                }
                showHideDeleteButton( skjemaId, isEdit );
                updateFormTitle( skjemaId, isEdit );
                el.style.display = "block";
                const firstElement = getFormFields(skjemaId)[0];
                if (firstElement) {
                    firstElement.focus();
                    firstElement.select();
                }
            } else {
                el.style.display = "none";
                clearForm(skjemaId);
            }
        }
        return false;
    }

    function clearForm(skjemaId) {
        var inputs = getFormFields(skjemaId, false);
        for (var i = 0; i < inputs.length; i++) { 
            inputs[i].value = ""; 
        }
        setFormAction(skjemaId, "save");
    }

    function showEdit(skjemaId, setId, cellClickEvent) {
        const rowElement = cellClickEvent.target.parentElement;
        const inputs = getFormFields(skjemaId, true);
        for (var i = 0; i < inputs.length; i++) {
            const ip = inputs[i];
            const ipName = ip.getAttribute("name");
            if (ipName) {
                const match = rowElement.getElementsByClassName(ipName);
                if (match && match.length > 0) {

                    ip.value = match[0].innerText;

                } else if (ipName === "ID") {

                    ip.value = setId;
                    showHideDeleteButton( skjemaId, !!setId );
                    updateFormTitle( skjemaId, true );

                } else {

                    if (ipName.indexOf("ID") === ipName.length - 2) {
                        selectComboValue(ip, ipName, rowElement);
                    }
                }
            }
        }
        showHide(skjemaId, true);
    }

    function selectComboValue(element, name, rowElement) {
        const cells = rowElement.getElementsByTagName("td");
        for (var i = 0; i < cells.length; i++) {
            const dataId = cells[i].getAttribute("data-id");
            const dataName = cells[i].getAttribute("data-name");
            if (dataId && dataName === name) {
                const options = element.getElementsByTagName("option");
                for (var o = 0; o < options.length; o++) {
                    const option = options[o];
                    if (option.value === dataId) {
                        option.selected = true;
                    }
                }
            }
        }
    }

    function showHideDeleteButton( skjemaId, bShow ) {
        const frm = document.getElementById(skjemaId);
        if (!frm) return;
        const buttons = frm.getElementsByTagName('button');
        if (buttons) {
            for (var i = 0; i < buttons.length; i++) {
                const btn = buttons[i];
                if (btn.classList.contains("delete")) {
                    btn.style.display = bShow ? "inline" : "none";
                }
            }
        }                        
    }

    function updateFormTitle( skjemaId, isEdit) {
        const el = document.getElementById(skjemaId);
        if (el) {
            const hd = el.getElementsByTagName("header");
            if (hd && hd.length > 0) {
                const hdElement = hd[0];
                const title = hdElement.getAttribute("data-title");
                hdElement.innerText = (isEdit ? app.lang.edit : app.lang.create) + ' ' + title;
            }
        }
    }

    function setFormAction( skjemaId, value ) {
        const flds = getFormFields(skjemaId, true);
        for (var i = 0; i < flds.length; i++) {
            const fld = flds[i];
            if (fld.getAttribute("name") === "action") {
                fld.value = value || "delete";
                return true;
            }
        }
        return false;
    }

    function setFormRecordID( skjemaId, value ) {
        const flds = getFormFields(skjemaId, true);
        for (var i = 0; i < flds.length; i++) {
            const fld = flds[i];
            if (fld.getAttribute("name") === "ID") {
                fld.value = value;
                return;
            }
        }
    }    

    function getFormFields(skjemaId, includeHidden) {
        var el = document.getElementById(skjemaId);
        if (el) {
            var response = [];
            var inputs = [ ... el.getElementsByTagName('input') ];
            for (var i = 0; i < inputs.length; i++) {
                const type = inputs[i].getAttribute("type");
                switch (type) {
                    case "submit":
                        break;
                    case "hidden":
                        if (includeHidden) response.push(inputs[i]);
                        break;
                    default:
                        response.push(inputs[i]);
                        break;
                }
            }
            var selects = [ ... el.getElementsByTagName('select')];
            return response.concat(selects);
        }
        return [];
    }

    function setTwoNumberDecimal(event) {
        this.value = parseFloat(this.value).toFixed(2);
    }

