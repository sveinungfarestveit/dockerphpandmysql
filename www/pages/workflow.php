<!doctype html>
<html lang="en">
    <head>
        <title>Workflow</title>
        <link rel="stylesheet" href="/styles.css">
        <?php require_once __DIR__ . "/../main.php"; ?>
    </head>
    <body>

        <?php
            echo createToolbar(App::$PAGES, "workflow");
        ?>
        <h1>Workflow</h1>

        <button onclick="showHide('workflow-form')">New workflow</button>

        <?php 
            echo createHiddenForm(App::class, "workflow-form", "Workflow", "workflow", [ "Step:number", "Name" ]);
            
            echo createHtmlTable(App::class, "select * from Workflow ORDER BY Step", "liste", "workflow-form", "Step", "Name");
        ?>

    </body>

</html>