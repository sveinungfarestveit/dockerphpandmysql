<!doctype html>
<html lang="en">
    <head>
        <title>Orders</title>
        <link rel="stylesheet" href="/styles.css">
        <?php require_once __DIR__ . "/../main.php"; ?>
    </head>
    <body>

        <?php
            echo createToolbar(App::$PAGES, "order");
        ?>
        <h1>Orders</h1>

        <button onclick="showHide('order')">New order</button>

        <?php  
            echo createHiddenForm(App::class, "order", "Order", "order",
                [ 
                    "OrderDate:date",
                    "CustomerID(Customer.ID,Customer.Name) => Customer", 
                    "ProductID(Product.ID,Product.Name) => Product",
                    "Quantity:money" 
                ]);

            echo createHtmlTable(App::class,  
                "SELECT C.Name as CustomerName, P.Code as ProductCode, P.Name as ProductName, O.*"
                . " FROM `Order` O"
                . " INNER JOIN `Customer` C ON O.CustomerID = C.ID"
                . " INNER JOIN `Product` P ON O.ProductID = P.ID"
                . " ORDER BY C.Name, OrderDate DESC", 
                "liste", "order",
                "OrderDate", "CustomerName(CustomerID) => Customer", "ProductCode(ProductID) => Product", "ProductName", "Quantity:money", "CreatedAt", "UpdatedAt");
            
        ?>

    </body>

</html>